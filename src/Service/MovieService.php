<?php

namespace App\Service;

use App\Entity\Actor;
use App\Entity\Movie;
use App\Entity\VideoFormat;
use App\Model\MySqlConnect;
use App\Repository\ActorsRepository;
use App\Repository\MovieRepository;
use App\Repository\VideoFormatRepository;

class MovieService
{
    /** @var MovieRepository */
    private MovieRepository $repo;

    /** @var VideoFormatRepository */
    private VideoFormatRepository $vRepo;

    /** @var ActorsRepository */
    private ActorsRepository $aRepo;

    public function __construct()
    {
        $this->repo = new MovieRepository(MySqlConnect::getInstance());
        $this->vRepo = new VideoFormatRepository(MySqlConnect::getInstance());
        $this->aRepo = new ActorsRepository(MySqlConnect::getInstance());
    }

    /**
     * @param array $data
     * @return Movie
     * @throws \Exception
     */
    public function createMovie(array $data): Movie
    {
        if (!isset($data['title'])) {
            throw new \Exception('Missing title argument!');
        }

        if (!isset($data['year'])) {
            throw new \Exception('Missing year argument!');
        }

        if (!isset($data['video_format'])) {
            throw new \Exception('Missing video_format argument!');
        }

        if (!is_numeric($data['year']) || $data['year'] < 1930 || date('Y') + 5 < $data['year']) {
            throw new \Exception('Invalid argument "year"');
        }

        if (!is_numeric($data['video_format'])) {
            throw new \Exception('Invalid argument "video_format"');
        }

        $movie = new Movie();
        $movie->setTitle(htmlspecialchars($data['title']));
        $movie->setYear($data['year']);

        $vf = $this->vRepo->find($data['video_format']);

        if (isset($data['actors'])) {
            $formattedActorsId = [];
            foreach ($data['actors'] as $datum) {
                $formattedActorsId[] = (int)$datum;
            }

            if ($formattedActorsId) {
                $actors = $this->aRepo->findByIds($formattedActorsId);

                if ($actors) {
                    foreach ($actors as $actor) {
                        $movie->addActor($actor);
                    }
                }
            }
        }

        if ($vf) {
            $movie->setVideoFormat($vf);
        }

        $this->repo->save($movie);

        return $movie;
    }

    public function import(string $path)
    {
        $handle = fopen($path, 'rb');
        $availableCol = ['Title', 'Release Year', 'Format', 'Stars'];
        $data = [];
        $singleData = [];
        if ($handle) {
            while (($line = fgets($handle, 1024)) !== false) {
                if ($line) {
                    foreach ($availableCol as $col) {
                        if (strpos($line, $col) !== false) {
                            $str = trim(str_replace($col . ':', '', $line));
                            $singleData[$col] = $str;
                            break;
                        }
                    }
                }

                if (count($singleData) === 4) {
                    $data[] = $singleData;
                    $singleData = [];
                }
            }
            fclose($handle);
        }

        if ($data) {
            $vfs = $this->vRepo->findAll();

            foreach ($data as $item) {
                $movie = new Movie();
                $movie->setTitle(trim($item['Title']));
                $movie->setYear((int)$item['Release Year']);

                foreach ($vfs as $vf) {
                    if (strtolower(trim($item['Format'])) === strtolower($vf->getName())) {
                        $movie->setVideoFormat($vf);
                    }
                }

                $actors = explode(',', $item['Stars']);

                foreach ($actors as $actor) {
                    $a = $this->findOrCreateActor(trim($actor));
                    $movie->addActor($a);
                }

                $this->repo->save($movie);
            }
        }
    }

    public function findOne(int $id): Movie
    {
        return $this->repo->find($id);
    }

    /**
     * @param $limit
     * @param $offset
     * @param $search
     * @return array
     */
    public function findList(int $limit, int $offset, ?string $search): array
    {
        return $this->repo->findList($limit, $offset, $search);
    }

    public function totalCount(?string $search)
    {
        return $this->repo->totalCount($search);
    }

    public function findActors(int $limit, int $offset)
    {
        return $this->aRepo->findBy($limit, $offset);
    }

    public function findAllVideoFormat(): array
    {
        return $this->vRepo->findAll();
    }

    public function removeMovie(int $id)
    {
        $this->repo->delete($id);
    }

    private function findOrCreateActor(string $name): Actor
    {
        $actor = $this->aRepo->findByName($name);

        if (!$actor) {
            $actor = new Actor();
            $actor->setName($name);
            $actor = $this->aRepo->save($actor);
        }


        return $actor;
    }
}