<head>
    <title><?php print $movie->getTitle(); ?></title>
    <?php require_once 'header.php'; ?>
</head>

<div class="container" id="wrapper">
    <ul class="list-group">
        <li class="list-group-item"><b>Id</b>: <?= $movie->getId() ?></li>
        <li class="list-group-item"><b>Title</b>: <?= $movie->getTitle() ?></li>
        <li class="list-group-item"><b>Year</b>: <?= $movie->getYear() ?></li>
        <li class="list-group-item"><b>Video</b>: <?= $movie->getVideoFormat()->getName() ?></li>
        <li class="list-group-item"><b>Actors</b>:
            <?php if ($movie->getActors()): ?>
                <?php foreach ($movie->getActors() as $actor): ?>
                    <div><?= $actor->getName() ?></div>
                <?php endforeach; ?>
            <?php else: ?>
                -
            <?php endif; ?>
        </li>
    </ul>

    <button id="btn-delete-movie" type="button" onclick="location.href='/index/delete/?id=<?= $movie->getId() ?>'"
            class="btn btn-danger">Delete
    </button>
    <button id="btn-back" type="button" onclick="location.href = '/'" class="btn btn-info">Back</button>
</div>