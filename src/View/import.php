<head>
    <title>Import movie</title>
    <?php require_once 'header.php'; ?>
</head>

<div class="container" id="wrapper">
    <form action="/index/import/" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">File:</label>
            <input type="file" name="file" class="form-control" required="required"
                   id="file">
        </div>
        <input type="hidden" name="submit" value="on">
        <button id="btn-add" type="submit" class="btn btn-primary">Import</button>
        <button id="btn-back" type="button" onclick="location.href = '/'" class="btn btn-info">Back</button>
    </form>
</div>