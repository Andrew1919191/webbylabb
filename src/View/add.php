<head>
    <title>Add movie</title>
    <?php require_once 'header.php'; ?>
</head>

<div class="container" id="wrapper">
    <form action="/index/create/" method="post">
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" class="form-control" required="required" placeholder="Enter title"
                   id="title">
        </div>
        <div class="form-group">
            <label for="year">Year:</label>
            <input type="number" min="1930" max="<?php echo date('Y') + 5; ?>" required="required" name="year"
                   class="form-control"
                   placeholder="Enter year" id="year">
        </div>
        <div class="form-group">
            <label for="video_format">Video format:</label>
            <select name="video_format" class="form-control" required="required">
                <?php $i = 0;
                foreach ($formats as $format): ?>
                    <option value="<?= $format->getId(); ?>" <?php echo $i === 0 ?  'selected' : ''; $i++; ?> ><?= $format->getName(); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="video_format">Actors:</label>
            <select name="actors[]" class="form-control" multiple>
                <?php foreach ($actors as $actor): ?>
                    <option value="<?= $actor->getId(); ?>"><?= $actor->getName(); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <input type="hidden" name="submit" value="on">
        <button id="btn-add" type="submit" class="btn btn-primary">Add</button>
        <button id="btn-back" type="button" onclick="location.href = '/'" class="btn btn-info">Back</button>
    </form>
</div>