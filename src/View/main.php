<head>
    <title>Test</title>
    <?php require_once 'header.php'; ?>
</head>

<div class="container">
    <form id="search-form" class="form-inline">
        <div class="form-group mb-2">
            <label for="search-film">Search</label>
            <input type="text" class="form-control" id="search-film" name="search" value="<?= $search ?>" placeholder="Enter the movie title">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>
    <a href="/index/create/">Add new movie</a><br/>
    <a href="/index/import/">Import movies</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Year</th>
            <th scope="col">Video format</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        /** @var \App\Entity\Movie $item */
        foreach ($entities as $item): ?>
            <tr>
                <th scope="row"><?= $item->getId(); ?></th>
                <td><?= $item->getTitle(); ?></td>
                <td><?= $item->getYear(); ?></td>
                <td><?= $item->getVideoFormat()->getName(); ?></td>
                <td>
                    <button type="button" class="btn btn-info" onclick="location.href='/index/show/?id=<?= $item->getId() ?>'">Show</button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>