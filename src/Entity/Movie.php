<?php

namespace App\Entity;

class Movie
{
    /** @var int|null */
    private ?int $id;

    /** @var string */
    private string $title;

    /** @var int */
    private int $year;

    /** @var VideoFormat */
    private VideoFormat $videoFormat;

    /** @var Actor[] */
    private array $actors = [];

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param VideoFormat $format
     */
    public function setVideoFormat(VideoFormat $format): void
    {
        $this->videoFormat = $format;
    }

    /**
     * @return VideoFormat
     */
    public function getVideoFormat(): VideoFormat
    {
        return $this->videoFormat;
    }

    /**
     * @param Actor $actor
     */
    public function addActor(Actor $actor): void
    {
        $this->actors[] = $actor;
    }

    /**
     * @return Actor[]
     */
    public function getActors(): array
    {
        return $this->actors;
    }

    /**
     * @param Actor $actor
     */
    public function removeActor(Actor $actor): void
    {
        foreach ($this->actors as $key => $a) {
            if ($a->getId() === $actor->getId()) {
                unset($this->actors[$key]);
            }
        }
    }
}