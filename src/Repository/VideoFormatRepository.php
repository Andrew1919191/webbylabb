<?php

namespace App\Repository;

use App\Entity\VideoFormat;
use App\Model\IDbConnect;
use PDO;

class VideoFormatRepository implements Repository
{
    /** @var PDO */
    private $db;

    public function __construct(IDbConnect $db)
    {
        $this->db = $db->getConnect();
    }


    public function findAll(): array
    {
        $conn = $this->db->prepare("SELECT id, alias, name FROM video_format");
        $conn->execute();
        return $conn->fetchAll(PDO::FETCH_CLASS, VideoFormat::class);
    }

    public function save($object)
    {
        $this->validObject($object);
    }

    public function delete(int $id)
    {

    }

    public function find(int $id): VideoFormat
    {
        $conn = $this->db->prepare("SELECT id, alias, name FROM video_format WHERE id = :id");
        $conn->bindParam(':id', $id, PDO::PARAM_INT);
        $conn->execute();
        return $conn->fetchObject(VideoFormat::class);
    }

    public function update($object, array $data)
    {
        $this->validObject($object);
    }

    public function validObject($object): void
    {
        if (!(is_object($object) && get_class($object) === VideoFormat::class)) {
            throw new \Exception('Invalid class in repository');
        }
    }
}