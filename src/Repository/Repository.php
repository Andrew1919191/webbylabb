<?php


namespace App\Repository;


use App\Model\IDbConnect;

interface Repository
{
    public function __construct(IDbConnect $connect);

    public function find(int $id);

    public function save($object);

    public function delete(int $id);

    public function validObject($object);
}