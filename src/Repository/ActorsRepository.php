<?php

namespace App\Repository;

use App\Entity\Actor;
use App\Entity\VideoFormat;
use App\Model\IDbConnect;
use PDO;

class ActorsRepository implements Repository
{
    /** @var PDO */
    private $db;

    public function __construct(IDbConnect $db)
    {
        $this->db = $db->getConnect();
    }

    public function findBy($limit, $offset)
    {
        $conn = $this->db->prepare("SELECT id, name FROM actors LIMIT :limit OFFSET :offset");
        $conn->bindParam(':limit', $limit, PDO::PARAM_INT);
        $conn->bindParam(':offset', $offset, PDO::PARAM_INT);
        $conn->execute();
        return $conn->fetchAll(PDO::FETCH_CLASS, Actor::class);
    }

    public function findByIds(array $ids)
    {
        $ids = '(' . implode(',', $ids) . ')';
        $ids = htmlspecialchars($ids);

        $conn = $this->db->prepare("SELECT id, name FROM actors WHERE id IN $ids");
        $conn->bindParam(':ids', $ids);
        $conn->execute();
        return $conn->fetchAll(PDO::FETCH_CLASS, Actor::class);
    }

    public function findByName(string $name)
    {
        $conn = $this->db->prepare("SELECT id, name FROM actors WHERE lower(name) = :name");
        $conn->bindParam(':name', $name);
        $conn->execute();
        return $conn->fetchObject(Actor::class);
    }

    /**
     * @param $object
     * @throws \Exception
     */
    public function save($object)
    {
        $this->validObject($object);

        $this->db->beginTransaction();
        try {
            $stmt = $this->db->prepare('INSERT INTO actors (name) VALUES (:name)');
            $name = $object->getName();
            $stmt->bindParam(':name', $name);
            $stmt->execute();
            $id = $this->db->lastInsertId();
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw new \Exception('Save actor to movie... Something went wrong...');
        }

        return $this->find($id);
    }

    public function find(int $id)
    {
        $conn = $this->db->prepare("SELECT id, name FROM actors WHERE id =:id");
        $conn->bindParam(':id', $id);
        $conn->execute();
        return $conn->fetchObject(Actor::class);
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }

    public function validObject($object)
    {
        if (!(is_object($object) && get_class($object) === Actor::class)) {
            throw new \Exception('Invalid class in repository');
        }
    }
}