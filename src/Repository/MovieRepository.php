<?php

namespace App\Repository;

use App\Entity\Actor;
use App\Entity\Movie;
use App\Entity\VideoFormat;
use App\Model\IDbConnect;
use PDO;

class MovieRepository implements Repository
{
    /** @var PDO */
    private $db;

    public function __construct(IDbConnect $db)
    {
        $this->db = $db->getConnect();
    }

    public function findList($limit, $offset, $search = '')
    {
        $sql = 'SELECT m.id, m.title, m.year, vf.name as vfName, vf.alias as vfAlias, vf.id as vfId
        FROM movie m
        INNER JOIN video_format vf on m.video_format_id = vf.Id';

        if ($search) {
            $sql .= '
            LEFT JOIN movie_actors ma ON ma.movie_id = m.id
            LEFT JOIN actors a ON a.id = ma.actor_id
            WHERE lower(m.title) like :search OR lower(a.name) like :search';
        }

        $sql .= '
        GROUP BY m.id
        ORDER BY m.title, m.year, m.id
        LIMIT :limit OFFSET :offset';
        $conn = $this->db->prepare($sql);
        $conn->bindParam(':limit', $limit, PDO::PARAM_INT);
        $conn->bindParam(':offset', $offset, PDO::PARAM_INT);

        if ($search) {
            $search = '%' . strtolower($search) . '%';
            $conn->bindParam(':search', $search, PDO::PARAM_STR);
        }

        $conn->execute();
        $rows = $conn->fetchAll(PDO::FETCH_OBJ);

        return $this->processObjects($rows);
    }

    public function totalCount($search = '')
    {
        $sql = 'SELECT COUNT(m.id) AS total
        FROM movie m
        INNER JOIN video_format vf on m.video_format_id = vf.Id';

        if ($search) {
            $sql .= '
            LEFT JOIN movie_actors ma ON ma.movie_id = m.id
            LEFT JOIN actors a ON a.id = ma.actor_id
            WHERE lower(m.title) like :search OR lower(a.name) like :search';
        }

        $conn = $this->db->prepare($sql);

        if ($search) {
            $search = '%' . strtolower($search) . '%';
            $conn->bindParam(':search', $search, PDO::PARAM_STR);
        }

        $conn->execute();
        return $conn->fetchColumn();
    }

    /**
     * @throws \Exception
     * @var Movie $object
     */
    public function save($object)
    {
        $this->validObject($object);

        $this->db->beginTransaction();
        try {
            $stmt = $this->db->prepare("INSERT INTO movie (title, year, video_format_id) VALUES (:title, :year, :video_format_id)");
            $title = $object->getTitle();
            $stmt->bindParam(':title', $title);
            $year = $object->getYear();
            $stmt->bindParam(':year', $year);
            $id = $object->getVideoFormat()->getId();
            $stmt->bindParam(':video_format_id', $id);
            $stmt->execute();
            $id = $this->db->lastInsertId();
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw new \Exception('Save movie... Something went wrong...');
        }

        if ($object->getActors()) {
            $this->db->beginTransaction();
            try {
                foreach ($object->getActors() as $actor) {
                    $stmt = $this->db->prepare('INSERT INTO movie_actors (movie_id, actor_id) VALUES (:movie_id,:actor_id)');
                    $stmt->bindParam(':movie_id', $id);
                    $id2 = $actor->getId();
                    $stmt->bindParam(':actor_id', $id2);
                    $stmt->execute();
                }
                $this->db->commit();
            } catch (\Exception $e) {
                $this->db->rollBack();
                throw new \Exception('Save actor to movie... Something went wrong...');
            }
        }
    }

    public function delete(int $id)
    {
        $movie = $this->find($id);
        if (!$movie) {
            throw new \Exception('Movie not found by id');
        }

        $this->db->beginTransaction();
        try {
            $conn = $this->db->prepare('DELETE FROM movie_actors WHERE movie_id = :id');
            $conn->bindParam(':id', $id, PDO::PARAM_INT);
            $conn->execute();

            $conn = $this->db->prepare('DELETE FROM movie WHERE id = :id');
            $conn->bindParam(':id', $id, PDO::PARAM_INT);
            $conn->execute();
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw new \Exception('Something went wrong!');
        }
    }

    public function find(int $id): Movie
    {
        $sql = 'SELECT m.id, m.title, m.year, vf.name as vfName, vf.alias as vfAlias, vf.id as vfId, GROUP_CONCAT(CONCAT(a.id,":",a.name)) as actorName
        FROM movie m
        INNER JOIN video_format vf ON m.video_format_id = vf.Id
        LEFT JOIN movie_actors ma ON ma.movie_id = m.id
        LEFT JOIN actors a ON a.id = ma.actor_id
        WHERE m.id = :id
        GROUP BY m.id';
        $conn = $this->db->prepare($sql);
        $conn->bindParam(':id', $id, PDO::PARAM_INT);
        $conn->execute();
        $row = $conn->fetchObject();
        $row = $this->processObjects([$row]);

        return $row[0];
    }

    public function validObject($object): void
    {
        if (!(is_object($object) && get_class($object) === Movie::class)) {
            throw new \Exception('Invalid class in repository');
        }
    }

    /**
     * @param array $movies
     * @return array
     */
    private function processObjects(array $movies): array
    {
        $objects = [];
        foreach ($movies as $movie) {
            $vf = new VideoFormat();
            $vf->setId($movie->vfId);
            $vf->setAlias($movie->vfAlias);
            $vf->setName($movie->vfName);

            $m = new Movie();
            $m->setId($movie->id);
            $m->setTitle($movie->title);
            $m->setYear($movie->year);
            $m->setVideoFormat($vf);

            if (isset($movie->actorName)) {
                $actors = explode(',', $movie->actorName);

                foreach ($actors as $actor) {
                    $parsed = explode(':', $actor);
                    $a = new Actor();
                    $a->setId($parsed[0]);
                    $a->setName($parsed[1]);
                    $m->addActor($a);
                }
            }

            $objects[] = $m;
        }

        return $objects;
    }
}