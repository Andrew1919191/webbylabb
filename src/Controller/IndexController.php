<?php

namespace App\Controller;

use App\Model\Render;
use App\Service\MovieService;
use DOMDocument;

class IndexController extends Render
{
    /**
     * @var MovieService
     */
    private MovieService $service;

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->service = new MovieService();
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $search = isset($_GET['search']) ? trim(htmlspecialchars($_GET['search'])) : null;
        $data = $this->service->findList(100, 0, $search);
        $totalCount = $this->service->totalCount($search);

        return $this->render('../View/main.php', ['entities' => $data, 'search' => $search, 'total' => $totalCount]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionShow(): string
    {
        $id = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : null;

        if (!$id) {
            throw new \Exception('Missing argument \'id\'');
        }
        $data = $this->service->findOne($id);
        return $this->render('../View/show.php', ['movie' => $data]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCreate(): string
    {
        $videFormats = $this->service->findAllVideoFormat();
        $actorsList = $this->service->findActors(50, 0);

        if (isset($_POST['submit'])) {
            $this->service->createMovie($_POST);
            header('Location: /');
        }

        return $this->render('../View/add.php', ['formats' => $videFormats, 'actors' => $actorsList]);
    }

    public function actionImport()
    {
        if (isset($_POST['submit'])) {
            $path = $_FILES['file']['tmp_name'];
            if (!$path) {
                throw new \Exception('File was not uploaded. Reload page and try again.');
            }
            $this->service->import($path);
            header('Location: /');
        }

        return $this->render('../View/import.php');
    }

    /**
     * @throws \Exception
     */
    public function actionDelete(): void
    {
        $id = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : null;

        if (!$id) {
            throw new \Exception('Missing argument \'id\'');
        }

        $this->service->removeMovie($id);
        header('Location: /');
    }
}