<?php

namespace App;

use App\Service\MovieService;

class Routing
{
    public static function buildRoute(): void
    {
        $controllerName = "IndexController";
        $action = "index";
        $r = parse_url($_SERVER['REQUEST_URI']);
        $route = explode("/", $r['path']);

        if ($route[1] !== '') {
            $controllerName = ucfirst($route[1] . "Controller");
        }

        if (isset($route[2]) && $route[2] !== '') {
            $action = ucfirst($route[2]);
        }

        if (file_exists('Controller/' . $controllerName . '.php')) {
            try {
                $action = 'action' . $action;
                $class = 'App\Controller\\' . $controllerName;
                $c = new $class();
                echo $c->$action();
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        } else {
            require_once 'View/404.php';
        }
    }
}