<?php

namespace App\Model;

interface IDbConnect
{
    public function getConnect();
}