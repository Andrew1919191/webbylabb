<?php

namespace App\Model;

class DBConfig implements IDBConfig, ISingleton
{
    private static ?self $instance = null;

    /** @var string */
    private string $host;

    /** @var string */
    private string $dbName;

    /** @var string */
    private string $password;

    /** @var string */
    private string $username;

    /**
     * DBConfig constructor.
     */
    private function __construct()
    {
        $this->parseConfig();
    }

    /**
     * @return DBConfig
     */
    public static function getInstance(): self
    {
        if (!self::$instance) {
            return new self;

        }

        return self::$instance;
    }

    private function parseConfig(): void
    {
        try {
            $config = json_decode(file_get_contents(__DIR__ . '/../config.json'), false, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new \Exception($e->getMessage());
        }

        $this->host = $config->host;
        $this->dbName = $config->dbName;
        $this->password = $config->password;
        $this->username = $config->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getDBName(): string
    {
        return $this->dbName;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    private function __clone()
    {

    }
}