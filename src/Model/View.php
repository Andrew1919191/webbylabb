<?php


namespace App\Model;


interface View
{
    public function render(string $path, array $data = []);
}