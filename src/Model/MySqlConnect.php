<?php


namespace App\Model;

use PDO;

class MySqlConnect implements IDbConnect, ISingleton
{
    private static ?self $instance = null;

    /** @var PDO */
    private $pdo;

    private function __construct()
    {
        $config = DBConfig::getInstance();
        $this->connect($config);
    }

    /**
     * @return MySqlConnect
     */
    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }


    public function getConnect(): PDO
    {
        return $this->pdo;
    }

    private function connect(IDBConfig $config): void
    {
        $this->pdo = new PDO("mysql:host={$config->getHost()};dbname={$config->getDBName()}", $config->getUsername(), $config->getPassword());
    }

    private function __clone()
    {
        echo 'You can\'t clone this object';
    }

    private function __wakeup()
    {
    }
}