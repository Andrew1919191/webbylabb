<?php


namespace App\Model;


abstract class Render implements View
{
    public function render(string $path, array $data = [])
    {
        extract($data, EXTR_OVERWRITE);
        ob_start();
        require_once __DIR__ . '/' . $path;
        return ob_get_clean();
    }
}