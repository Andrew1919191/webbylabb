<?php

namespace App\Model;

interface IDBConfig
{
    public function getHost(): string;

    public function getUsername(): string;

    public function getPassword(): string;

    public function getDBName(): string;
}