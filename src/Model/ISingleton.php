<?php

namespace App\Model;

interface ISingleton
{
    public static function getInstance(): self;
}