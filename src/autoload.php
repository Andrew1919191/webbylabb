<?php

namespace App;

spl_autoload_register(static function ($class) {
    if (strpos($class, 'App\\') !== false) {
        $class = preg_replace('/' . __NAMESPACE__ . '\\\\/', '', $class, 1);
        $class = str_replace('\\', '/', $class);

        include __DIR__ . '/' . $class . '.php';
    }
});