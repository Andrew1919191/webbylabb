<?php

namespace App;

require_once __DIR__ . '/autoload.php';

error_reporting(E_ALL);
ini_set("display_errors", 1);

Routing::buildRoute();
